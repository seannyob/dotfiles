## Userspace Anacron ##

Requires entires in your user level cron.
Run `crontab -e` to invoke your $EDITOR and add these lines:

```
# this tells anacron to use the $HOME/.anacron user dir
01 * * * * /usr/sbin/anacron -t /home/sean/.anacron/anacrontab -S /home/sean/.anacron/spool

# run hourly to process all jobs
01 * * * * nice run-parts /home/sean/.anacron/cron.hourly
```

### empty_trash ###

The script located at cron.daily/empty_trash purges `.local/share/Trash` 
every day. It deletes files which are older than a week from today...

If you're using zfs with sanoid or auto snapshots, this is sane.

If you're using our configs, remember that `restore` in any dir will
restore recently rm'd files from the trash.

### trash_downloads ###

The script located at cron.weekly/trash_downloads moves downloaded files
from the filesystem to the trash on a weekly basis to keep the Downloads
directory clean.

If you are using ZFS snapshots this is sane, particularly if your $HOME
directory is on a small and fast NVMe drive. Which it should be...

*More to come*
