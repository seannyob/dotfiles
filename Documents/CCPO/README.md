
Interactive ZSH is rather different than what you have used before.

There is no need to type the `cd` command: 
                
`..`    goes up one directory
`...`   goes up two directories

typing a directory `name` will take you there, assuming the path is sane.
typing cd followed by a directory abbreviation string then hitting TAB
will autoexpand deep directories. 

if want to go to a directory you've been to before, just type `z $DIR`.

ZSH will try to open filenames in accordance to their suffixes with sane defaults:
Certain files will open automagically if typed: 
If you type `foo.html`, it will open in your $BROSWER.
If you type `foo.py`, it will open in your $EDITOR.
If you set things properly in `.zsh_variables`, everything should work fine.

This is supposed to just "make sense". If it annoys you, it's easy to turn it off!

The following aliases work in ZSH if you are using sean's configuation files.

**~**     `cd`, aka, go $HOME

**a**     Short for `ansible`
**b**     Short for `bpytop`, a very fancy monitor.
**c**     Clear your terminal.
**cdn**   Clear your terminal, and invoke an ODU version of Neo from the Matrx.
**e**     Edit a file with your $EDITOR.
**g**     Short for `git`.
**h**     Print cmd history.
**h G**   Prints a cmd history, pipes it to grep, and searches for its args
**i**     Installs a package (via apt, macports, etc)
**m**     Open a process monitor. this opens `glances`. hit h for help.
**s**     SSH with X to any server or remote rig. please use .ssh/config.
**t**     `tail -f`. Useful to keep an eye on log or debug files, as they grow.
**x**     Extract *any* compressed file.
**z**     CD to any directory you've visited before.
>
**upgrade**   Bring your env and system up to date with upstream bugfixes. 
**usage**     Print the usage info for all users on the system.
**quotas**    Print the quotas for everyone.
**pool**      Display the calypso zfs array health. For transparency, obvs.

It should always look like this or be in maintenance:
 ```
   pool: calypso-data
  state: ONLINE
   scan: scrub repaired 0B in 0 days 08:23:12 with 0 errors on Sun Feb 28 09:05:17 2021
 config:
 
 	NAME                                                  STATE     READ WRITE CKSUM
 	calypso-data                                          ONLINE       0     0     0
 	  raidz2-0                                            ONLINE       0     0     0
 	    scsi-35000c500a24501a7                            ONLINE       0     0     0
 	    scsi-35000c500a24606fa                            ONLINE       0     0     0
 	    scsi-35000c500a244fef0                            ONLINE       0     0     0
 	    scsi-35000c500a2453ae6                            ONLINE       0     0     0
 	    scsi-35000c500a245bac3                            ONLINE       0     0     0
 	    scsi-35000c500a2450242                            ONLINE       0     0     0
 	    scsi-35000c500a245dd00                            ONLINE       0     0     0
 	    scsi-35000c500a2453d7a                            ONLINE       0     0     0
 	    ata-ST8000AS0002-1NA17Z_Z840ZF6G                  ONLINE       0     0     0
 	    ata-ST8000AS0002-1NA17Z_Z840ZEZ2                  ONLINE       0     0     0
 	    ata-ST8000AS0002-1NA17Z_Z840ZF0L                  ONLINE       0     0     0
 	    ata-ST8000AS0002-1NA17Z_Z840ZEX4                  ONLINE       0     0     0
 	    ata-ST8000AS0002-1NA17Z_Z840YXY8                  ONLINE       0     0     0
 	    ata-ST8000AS0002-1NA17Z_Z840YXW1                  ONLINE       0     0     0
 	logs	
 	  ata-HDSTOR_-_HSAV25ST250AX_HS1911111005B0BE4-part1  ONLINE       0     0     0
 	cache
 	  ata-HDSTOR_-_HSAV25ST250AX_HS1911111005B0BE4-part2  ONLINE       0     0     0
 	spares
 	  wwn-0x5000c500a24551a3                              AVAIL   
 
 errors: No known data errors
 ```

**rollbacks** Display the filesystem backups for the logged-in $USER.

They should look something like this:
 ```NAME                                                       CREATION
 calypso-data/sean@autosnap_2021-01-27_01:37:37_yearly      Tue Jan 26 20:37 2021
 calypso-data/sean@autosnap_2021-03-01_00:00:15_monthly     Sun Feb 28 19:00 2021
 calypso-data/sean@autosnap_2021-03-02_00:00:19_daily       Mon Mar  1 19:00 2021
 calypso-data/sean@autosnap_2021-03-02_05:00:21_hourly      Tue Mar  2  0:00 2021
 calypso-data/sean@autosnap_2021-03-02_05:30:25_frequently  Tue Mar  2  0:30 2021
 ```

## Working with files

**rm**            Remove a file/directory. Compliant with freedesktop standards.
**restore**       Restore a file/directory which was removed with `rm`. (from `.local/share/Trash`)
 **delete**       ***Really*** get rid of a file/directory. Calls `/bin/rm`. (Ask for more disk space before doing this)
 **copy**         Copy stuff from one place to another. 
                  `rcs` == `rsync-copy` == `rsync -avz --progress -h` *(rsync should almost always be called this way)*
 **move**         Move stuff from one place to another.
                  `rsync-move` == `rsync -avz --progress -h --remove-source-files` *move files/trees* _rather harsh..._
 **rupdate**      Rarely useful, use `git` lol
**synchronize**   syncs `here` to `there`, rtfm

## Working with SSH and its keys and configurations

**keys2upgrade**    Runs an analysis of the SSH keys in your .ssh directory and show you which ones should be upgraded from older algorithms. The newest is ed25519, to generate one use the following cmd:
**keygen**    Create ssh keys with the prefered algorithm.

## Working with packages on various platforms 

You may always attempt to do this, and may have local or system rights you do not know about.
Most of our scientists can do this on their own machines. sean can do it for you or temp
elevate your privilges if necessary. These should just work or provide feedback.

**install** like `i`, a shortcut to apt install, aka `agi`, installs a package 
            on macOS like port or brew
            on arch like pacman, etc.
            `install anypackage` must always work or please submit a bug fix request.

**remove**  `apt remove`, aka `agr`, i.e. removes a package, as above on variant OSes
**search**  searches for available software.
**provides**    lists files provided by a software package.

*TIPS*

    1 if you are working in a directory constantly, you can just z dir
        do not bother cd'ing to a deep dir tree after the first time.
    2 set your $SHELL, $EDITOR & $BROWSWE, and things will work;
    3 use our cheatsheats & workflows and we can walk you through
        anything, or help you automate virtually anything.


## Editors

Do you what you want, or if you want to hack code like cool kids, just use nvim
with airline like we do and we'll teach you our workflows. 
We can do it over lunch and you will be very, very, very happy.
If you can type, you should be using neovim or emacs. Atom.io is grahical,
and offers keybindings for anything, runs on everything, & is super cool.

(nano is not installed. you're a scientist. come on.)

sean uses neovim with vundle and nerdtree and jedi and airline and vi-mode in zsh
and vimium in firefox.

## Shells

A note on shells: you may, er, "use" any shell you like on our systems.
In reality, you'll simply be using our shells either way, in an arcane compliance mode
or in a sane contemporary mode.

/bin/sh and so on are actually provided by the debian almquist shell `dash`;
`/bin/bash` is around, but we only support interactive `zsh`.
If you were a bash user, nothing changes other than things got better.
`zsh` is completely backwards compliant.

If you were a csh user, well, we used CRAYS & VAX & stuff once too. Welcome to the future.
`/bin/tcsh` is there, so your csh scripts shall run, but don't use it as an interactive
login shell. Just don't. Please.

Your scripts, of course, will and should run and if they don't we'll make them do so for you.
But please note that interactive zsh is the default on all CCPO systems,
as well as the default in macOS.

## ENV

We run Oh My ZSH! and manage plugins & updates that way. Your shell environment should 
always return, in order:

An OS icon, in this case TUX; a directory location; a series of dots to the right;
the outcome of the last operation (green for true, red for false), its wallclock;
your identity, @ the machine;
the disk space on the slice of the system that you are currently on (upon login, your home dir);
the local time for calypso; and finally her IP address.

The prompt will escape to normal mode if you hit escape; if you are confused hit `i`;

The color of the prompt is another reminder about what you did last. If it is green 
your last command returned true; if it is red, your last command tossed a std error or retuned false;
if it points right you are in insert mode; if it points left, NORMAL mode.
You can disable this by deleting vi-mode.

## locale

Assume `en_UTF-8`.
If you would like to reconfigure your linux machines to any other international locale,
please let us know so that we may also load those configurations upstream for your convenience.
Locale requests are granted as a matter of course upon any request for any reason.

## git

We utilize `git` with `flow`. We can version any directory for you and assist you in
migration to `git` from any other system. We can provide you with roadmaps to 
intelligently and easily version your code, if you would like.

We may run a `git` server in the future, if users are interested.

## Software

If it is in the ubuntu multiverse and you have a scientific need for software, 
we should be able to meet it. 

If it is a git repo which requires administrative level access we will audit 
it and if it works we shall meet that request also;

We would rather you demand software than accept it although if you would like
to compute our way on macOS or linux we will teach you how. We are learning Windows in 
real time just like you, but we can try to help.

Software relased from our department will be sufficiently generic that it 
may be considered copyleft and uttely non-sensitive. If it helps you, great.
If you find something better for you, please let us know.

All machines will be administered with ansible and ssh if requested.
Everything should run ZSH.

## Operating Systems

We are agnostic so you may run what you choose or whatever you must for your 
scientific mission. We run open source software as a matter of policy, 
but are flexible.

We can build you automated workflows in any machine that speaks ZSH
upon request; we can do it the macOS or linux way upon request;
we can request Windows features from professionals or figure them out if
you give us the time as well.

We will try to help you regardless of the situation.

## 


