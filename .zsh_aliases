#!/bin/zsh
### The following ZSH options are sane.
# Er, sean thinks they are sane, so they are
# the CCPO defaults.

# ********** CONTENTS *******************
# *******
#
# - OPTIONS
# - ALIASES
# - ETC
#
# *******
# ***************************************

# ********** BEGIN OPTIONS **************
# *******
# *** These are sort of user level opts.
# *** If you hate it, just comment it out
# *******
# ***************************************

# The following makes searching for dotfiles
# such as ~you/*.txt easier
setopt dotglob

# Enables wildcard negation, etc.
# so ls ^foo* would list everything except
# strings matching foo*
setopt extendedglob

# This eliminates the need for the cd command,
# ie, typing the name of a directory simply
# takes you there. NOTE: "take" creates a directory
# and takes you there, too.
setopt autocd

# This one elminates the need to type a dot
# before you edit a dotfile or try to go into a dot dir
setopt globdots

# This option adds UTC timestamps to your zsh
# history, so if you grep it later, you'll know when
# you last ran it...
setopt extendedhistory

# This option sets an unilinear history,
# appending commands across all user sessions.
setopt appendhistory
    # and since that is enabled, let's ensure we don't
    # grow the file unnecessarily, the following are
    # sane:
    # expire duplicates first
    setopt HIST_EXPIRE_DUPS_FIRST
    # do not store duplications
    setopt HIST_IGNORE_DUPS
    #ignore duplicates when searching
    setopt HIST_FIND_NO_DUPS
    # removes blank lines from history
    setopt HIST_REDUCE_BLANKS

# !! repaces to the last cmd
setopt histverify


# ********** BEGIN ALIASES **************
# *******
# *** If you hate it, just comment it out
# *******
# ***************************************

# If you are a vi or vim user, plase migrate now.
# :h nvim-from-vim for more, although you're already using
# nvim, if you're using my systems.
# These are hard pushes in the zsh binary to call it faster.
export EDITOR=/usr/bin/nvim
export VISUAL=/usr/bin/nvim
alias neovim=/usr/bin/nvim
alias vim=/usr/bin/nvim
alias vi=/usr/bin/nvim
alias e=/usr/bin/nvim

# pretty
alias pretty='npx prettier'

# upgrade the system.
    alias upgrade="aguu -y && omz update && nvim +PluginUpdate +qall"

# If your server or workstation has zfs pools...
# display the zpool status
alias pool='zpool status'
# display everyone's datasets, listed as mount points
alias usage='zfs list'
# Show quotas for the data slices, for user convenience
alias quotas='zfs list -o name,quota,reservation'
# snapshots
alias rollbacks='zfs list -r -t snapshot -o name,creation | grep $USER'
#                      if you have your own zpool, change ^^^^^^^^^^^^^
#                      we use sanoid, but zfs-auto-snapshot works too.
#                      pick one and use it!!!!


# Stuff For Systems Administrators!
# *********************************
#
# if the user is in the systems group, these are helpful aliases for sysadmins.

# APT SHORTCUTS
# *************

# Install software from a repo already loaded:
# agi <pkgname>
alias install='agi'
alias i='install'

# Remove a package
alias remove='agr'

# Remove it and purge its known configs
alias purge='agp'

# search for packages
alias search='apt search'

# list files provided by a package
alias provides='apt-file list'

#  OTHER STUFF
#  ***********

# trigger visudo for editing files in /etc/sudoers.d/
alias v='sudo visudo'
# Dump a massive diagnostic to the window fast & now:
# (MAKE YOUR TERMINAL TALL)
alias mtail='sudo multitail /var/log/{syslog,auth.log,dpkg.log,kern.log,ufw.log}'
# quick tail
alias t='tail -f'

# monitor realtime traffic
alias iptraf='sudo /sbin/iptraf-ng'

# SSH STUFF
#
## Working with user keys
# get upgradable keys with the following
# this assumes sane defaults
# adjust for your env
# as of Jul2019 you must be running OpenSSH 6.5 or newer.
alias keys2upgrade='for keyfile in ~/.ssh/id_*; do ssh-keygen -l -f "${keyfile}"; done | uniq'

# set keygen to use the newest encryption algorithm,
# ed25519, by default, because we should Do the Right Thing.
alias keygen='ssh-keygen -o -a 100 -t ed25519 '

# Add known hosts to zsh auto-completion
zstyle -e ':completion:*:(ssh|scp|sftp|rsh|rsync):hosts' hosts 'reply=(${=${${(f)"$(cat {/etc/ssh_,~/.ssh/known_}hosts(|2)(N) /dev/null)"}%%[# ]*}//,/ })'

# ssh fast
alias s='ssh -Y'

##
# Generic stuff
# Aliases which are helpful for everyone
##

# print history
alias h=history

# rsync
# assumes omz with rsync, of course
alias rcs='rsync-copy'
alias copy='rcs'
alias rcm='rsync-move'
alias move='rcm'
alias rcu='rsync-update'
alias rupdate='rcu'
alias rss='rsync-synchronize' # rtfm!
alias synchronize='rss'

# git stuff
# this runs `git remote add origin`,
# for bitbucket cloud, or any other uri.
# pls use ssh keys and set up the remote repo first
# this blows away anything up there...
# Use for existing git & git flow projects
alias grao='git remote add origin'

alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME'

# apt install git-flow doesn't include
source /usr/share/zsh/vendor-completions/_git-flow

# TODO: alias newsoftware points to
# /usr/local/bin/newsoftware, which
# creates a software dir, creates a repo dir,
# inits the project, inits the flow,
# and optionally pushes up to a remote origin
# for sanity.

# More sane defaults for interactive shell users
alias cat=/usr/bin/batcat
alias more=less
alias less=cless    #color for less
alias diff=colordiff    #what's the point of diff if you can't see things
# remember that take creates a dir and takes you there
alias mkdir='mkdir -pv'
# disks -- display output is human readable, eg, in GB or TB
alias df='df -h'
alias du='du -ch'

# make info use vi keybindings.
# if you use emacs you don't want this.
alias info='info --vi-keys'

# make locate case insensitive by default
alias locate='locate -i'
# make grep case insensitive by default too
alias grep='grep -i'
# just to piss of the old school ppl
# set a better monitor. use glances, emacs, conky if you want Lua
alias top='glances'
# make 'm' a pointer to a great monitor
alias m='top'
# if you want a really rad monitor, &
# if you don't mind python, and have a huge terminal:
alias b='bpytop'
# make wget always try to recomplete in case it's a previously broken connection.
alias wget='wget -c'

# Run cdc or cdn to obfuscate a terminal immediately:
alias cdc='cd;clear'
alias cdn='cd;clear;neo'
alias cdm='cd;clear;morpheus'
alias cdt='cd;clear;trinity'

# just clear fast
alias c=clear

# trash makes rm freedesktop compliant. in other words,
# when you remove things, they will go to .local/share/Trash
# instead of /dev/null.
# Deletion never actually occurs anyway, so it shouldn't matter.
alias rm=/usr/bin/trash
alias restore=/usr/bin/trash-restore
# let's say users want to delete files for space. mmk, but zfs will
# roll them back anyway when they chnage their mind.
alias delete=/bin/rm
































































####
#### easter eggs
####
if [[ -x "$(command -v cmatrix)" ]];then
# this is just literally the greatest thing in the world.
# just do it in your longest *term. you'll thank me.
# change the color to your university's colors, or whatever.
# this one is optimized for ODU. GO MONARCHS!!!!!
    alias neo='cmatrix -abs -C blue'
# other cool versions:
    alias morpheus='cmatrix -abs -C green'
    alias trinity='cmatrix -abs -C magenta'
    alias cypher='cmatrix -abs -C red'
fi
if [[ -x "$(command -v cowsay)" ]];then
    alias moo='cowsay -w "Moo yourself."'
fi
if [[ -x "$(command -v telnet)" ]];then
    alias starwars='telnet towel.blinkenlights.nl'
fi
export ANSIBLE_COW_SELECTION=tux
