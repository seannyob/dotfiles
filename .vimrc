set encoding=utf-8
scripte utf-8

" .vimrc by seannyob
" for nvim5 & vundle
" might work for vim9
" linted with vint
" seannyob@icloud.com
" copyleft 23 Apr 2021
" links in biblio

" verbosity at max.

" HINTS:
"   comma-f         toggles the file browser
"   comma-m         toggles the  minimap
"   comma-c         turns off that annoying column line
"   comma-<SHIFT>C  turns it on again

"   everything autosaves
"   everything autolints
"   constantly...

" Introduction                                 intro_mark 'i
" ----------------------------------------------------------

" If you are a vim newbie, just use your up & down arrow keys
" to read this file. Nothing to fear. This file teaches
" you as you read it.

" this vimscipt version of $HOME/.vimrc should be called from:

"       $HOME/.config/nvim/init.vim

" ...so that it processes for both vim9 & nvim users.
" Please try NeoVim 5. This vimrc is compliant with
" nvim5 nightly builds.

" This configuration file is meant to:
"   set great default behavior for nvim users willing to try it,
"   give you lots of automation, hints and helper functions,
"   set up nvim as an IDE of sorts,
"   teach you how to use nvim,
"   help you learn how to configure nvim to your liking,
"   enable you to immediately toggle features on and off,
"       and so on.

" You can also navigate this file via the Table of Contents.
"
" To jump around and edit this file, use the marks in the TOC
" i.e., type:

"       't

" to go to the Table of Contents mark.

" If you get confused, hit <ESC> <apostrophe>-<t>
" to return to the Table of Contents.

" If you are new to nvim, you can type

"       :Tutor

" for training, at any time.

" If you are a vim user new to NeoVim, :h nvim-from-vim

" If you are an emacs user, why are you here? Welcome. Keep reading.
" Also, if you have a cool emacs for data scientists, please email us.

" Sections are folded. Jumping to any section opens the fold.

" If you are a vim newbie, just hit <SPACE> to open folds!
" Try it here:
" Hint: {{{ `zR` opens all folds & `zM` closes all folds.
" Or, type `zo` to open folds and `zc` to close them again.
" `za` toggles folds lightning fast in the blink of an eye! }}}

" If you have a touchscreen monitor, tapping it should place
" the cursor. Longer files will have a MiniMap zoomed view
" to the right, to help you navigate. Touch a location to go there.
" Drag your finger up and down the MiniMap to go places in the
" blink of an eye.

" PageUp and PageDown on your keyboard also work!
" Your mouse wheel should also work. :-D

" By now it should be obvious that in vimscript, lines that begin
" with a quotation mark   "   are comments!  ;)

"   TABLE OF CONTENTS

"   toc_mark      heading
"   --------      ---------------------------------
"   'i            Introduction
"   't            Table of Contents  (returns here)
"   'g            General Settings
"   'p            Python stuff
"   'k            <leader> keymapping
"   'm            Motion
"   'b            Tabs
"   'w            Window
"   'l            Plugins
"   'r            SuperTab
"   'q            MiniMap
"   'f            File browser
"   'a            Autocompletion for hackers
"   's            Linting & Style
"   'e            Etc
"   'o            biblio

" General settings {{{1                           gs_mark 'g
" ----------------------------------------------------------
" If you use vim9, stop doing that.
" LOL, j/k, just please uncomment the following:
" set nocompatible

" enable syntax highlighting
syntax on

" If you are using our colorschemes, settings in greenish
" yellow (the words after `set`) all have help menus.
" They could also be blue. You get the idea.

" You may access them using the command or colon
" mode by simply typing :h settingname

" :h[elp] whatever almost always works in nvim.

" execute is very useful.
" enable command autocomplete via menu when :e cmd is run
" ie, type :e <TAB>
set wildmenu                    " visual autocomplete for command
set wildignore+=.pyc,.swp,.zwc  " no binary files
set wildignorecase              " case insensitive searching

" tabs are set to four by default, and expand to spaces
" set for compliance with the PEP-8 standard
set expandtab
set smarttab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set autoindent

" linebreaks in the middle of a word drive me crazy
set linebreak

" vim redraws too much, or something
" if you have a wicked box comment this out for sure;
" i run nvim on a GPD Pocket 2, which is a four core
" micro rig with 8 GB of RAM, and it's fast.
" i suggest this is the best default setting.
set lazyredraw

" set line numbering on
set number

" in nvim, you type / to search and just start typing
" for whatever you are searching for...
" set searching to process chars as they are entered
set incsearch

" enable highlighting for searching
set hlsearch

" infer search case sensitivity from input
set infercase

" auto create undo file so that vim will undo
" even if you close & reopen
set undofile

" spellcheck stuff
" if you are running a debian flavor you're prob fine,
" otherwise you may need to run
"   mkspell ~/.vim/spell/en en_US
" for this to work.
set spelllang=en

" cursorline for scansion, you may hate this
" sorry i'm old and have progressive lenses
set cursorline

" most everyone will hate this; but because it
" is an occasionally useful option it is enabled here
" as an example. you may type comma-c to cancel this.
" or :set nocursorcolumn
set cursorcolumn

" you should use dark backgrounds, natch
" NOTE: this is a default in nvim
" set background=dark

" this makes backspace seem sane in insert mode
set backspace=indent,eol,start

" better directories for vim's crufty stuff
" for vim9:
"set backupdir=~/.cache/vim
"set directory=~/.cache/vim
" use default values for nvim :h vim-differences.

" just do it.
set backup
set writebackup

let g:auto_save = 1  " enable AutoSave on Vim startup
" this is annoying as hell until it's useful.
" i do this, but please understand that it's
" autowriting your buffers constantly.

" this is even more aggressive; autosaving literally
" all of your open nvim files on many event changes.
" i do this, but you do you, boo.
let g:auto_save_write_all_buffers = 1  " write all open buffers, i.e. :wa

" update the current working dir automatically
set autochdir

" autoread and autowrite are off by default.
" :h autoread for more.
set autoread
set autowriteall

" This function enables autosaving of folds & marks
augroup asfm    " {{{2
                " ^^^^
                " begin a level 2 fold syntax
    autocmd!
    set viewoptions-=options
    autocmd BufWritePost *
    \   if expand('%') != '' && &buftype !~ 'nofile'
    \|      mkview
    \|  endif
    autocmd BufRead *
    \   if expand('%') != '' && &buftype !~ 'nofile'
    \|      silent loadview
    \|  endif
                " close the level 2 fold syntax
augroup END     " }}}

" Most terminals support mice:
if has('mouse')
    " enable in all modes
    set mouse=a
    " let the mouse focus vim windows
    set mousefocus
    " no idea why this isn't working...
    set mousemodel=popup_setpos
    " Display 5 lines above/below the cursor when scrolling
    " with a mouse.
    set scrolloff=10
endif
" Suggested termintals: kitty, iTerm2, gnome-terminal, xfce4-terminal
" If you love a different terminal, please email us and we might
" test it for you.

" Speed up scrolling in vim9
" this is on by default in nvim
"set ttyfast

" Folds are a great way to organize your code,
" docs, or buffers.
set foldenable

" required for setmarks(), but you could change this
" in the vim after settings if you hate it.
" i like this because the default fold delimiter {{{ }}}
" is easy to remember.
set foldmethod=marker

" show the statusline
set laststatus=2
" .......................................................... }}}

" Python stuff {{{1                           python_mark 'p
" ----------------------------------------------------------
" path to python3 on debians
let g:python3_host_prog = '/usr/bin/python3'
" if your OS has a different sane path, that feature is coming.
" see .zsh_variables

" disable python2
let g:loaded_python_provider = 0
" .......................................................... }}}

" <leader> keymapping {{{1                    leader_mark 'k
" ----------------------------------------------------------
" it is ever so common to change the leader key
" to the comma key, for good reasons.
" adjust to your liking.

" NOTE: by default the <leader> key is \ ... FYI, if
" you prefer this delete or comment the next line.

let mapleader = ','

" some wizards also choose the space bar...funky.
"let mapleader = '<SPACE>'

" vim-which-key will display a popup of leader key
" shortcuts if you tap the leader key and wait
nnoremap <silent> <leader> :WhichKey ','<CR>

" turn off search highlight fast with <COMMA><SPACE>
" ok so vim highlights your / search until you manually clear
" so this remap lets you do comma-space to clear the
" highlights. NOTE :nohlsearch will always work
" comma-space
nnoremap <leader><space> :nohlsearch<CR>

" most hackers like to turn this on & off fast
" because leaving it on fscks your flow.
" fscking turn off cursorcolumn fast, comma-c
map <leader>c :set nocursorcolumn<CR>
" turn it back on, comma-<SHIFT>C
map <leader>C :set cursorcolumn<CR>

" toggle line numbers for copy/paste with mice
" comma-n
map <leader>n :set nonumber<CR>
" comma-shift-n
map <leader>N :set number<CR>

" toggle spellcheck with comma-s
nnoremap <leader>s :set spell<CR>
" turn off with comma-<SHIFT>S
nnoremap <leader>S :set nospell<CR>

" fsck all this and just quit fast, discards changes
" since you last wrote the buffer
" this hard quits all your buffers...
" comma-q
nnoremap <leader>q :qall!<CR>

" write the buffer and keep it open
" this writes your open buffers...
" comma-w
nnoremap <leader>w :wall!<CR>

" write & get out
" this writes everything and exists to shell...
" comma-x
nnoremap <leader>x :xall!<CR>
" .......................................................... }}}

" Motion {{{1                                 motion_mark 'm
" ----------------------------------------------------------
" ^ and $ move to beginning/end of a line respectfully
" map these to use shift-B & shift-E to move to Beginning/End of line
nnoremap B ^
nnoremap E $

" HINTS:
" if you are a vim newbie, here's a primer:

" <ESC> triggers NORMAL mode

" dd    deletes (cuts) a line
" yy    yanks (copies) it
" p     pastes it back in

" dw    deletes a word
" x     deletes a character
" d$    deletes from cursor to EOL
" ,d    comma-d does the same thing,
" C     er, <SHIFT>C does the same thing
"           & then tosses you into INSERT mode.

" i     triggers INSERT mode at the cursor

"   Remember: :h[elp] whatever to access
"   the nvim help system!

"   https://devhints.io/vim
"   is a great cheatsheet on the web.

" comma-d to delete to eol from cursor position
nnoremap <leader>d d$
" .......................................................... }}}

" Tabs {{{1                                     tabs_mark 'b
" ----------------------------------------------------------
" trigger tabedit and so you can write a new filename
" comma-t
nnoremap <leader>t :tabe<space>

" open a new tab & blank buffer
" comma-shift-T
nnoremap <leader>T :tabnew<CR>

" Navigating tabs

" If you hold down ALT, the l/r arrow keys
" will move you through nvim tabs.

" CTL+PageUp and CTL+PageDown work,
" as does gt and gT in Normal mode.
" However, imho, ALT+RightArrow & ALT+LeftArrow are faster.
nnoremap <silent><A-Right> :tabn<CR>
nnoremap <silent><A-Left> :tabp<CR>
" .......................................................... }}}

" Window navigation {{{1                      window_mark 'w
" ----------------------------------------------------------
" Remember: If you hold down ALT, all of the arrow keys
" will move you through nvim windows and tabs!

" Tabs go right & left, we'll make windows up and down
" this replicates <ctl-w> w
map <A-Up> :wincmd w<CR>
map <A-Down> :wincmd W<CR>
" .......................................................... }}}

" Plugins {{{1                               plugins_mark 'l
" ----------------------------------------------------------
"  NOTE: We are plugin manager agnotisc. If you hate Vundle,
"  that's fine, please file a hotfix requst and we'll try to
"  adjust the environment to accomdate it. I kinda <3 Vundle.
filetype off
set runtimepath+=~/.vim/bundle/Vundle.vim
call vundle#begin()

    " YOU MUST PUT PLUGINS HERE FOR VUNDLE TO HANDLE THEM
    " BEGIN PLUGIN LIST

    Plugin 'VundleVim/Vundle.vim'
    Plugin 'flazz/vim-colorschemes'
    Plugin 'scrooloose/nerdtree'
    Plugin 'vim-airline/vim-airline-themes'
    Plugin 'vim-airline/vim-airline'
    Plugin '907th/vim-auto-save'
    Plugin 'dense-analysis/ale'
    Plugin 'Xuyuanp/nerdtree-git-plugin'
    Plugin 'ryanoasis/vim-devicons'
    Plugin 'davidhalter/jedi-vim'
    Plugin 'Shougo/deoplete.nvim'
    Plugin 'tpope/vim-eunuch'
    Plugin 'tpope/vim-fugitive'
    Plugin 'tmsvg/pear-tree'
    Plugin 'liuchengxu/vim-which-key'
    Plugin 'osamuaoki/vim-spell-under'
    Plugin 'wfxr/minimap.vim'

    " END PLUGIN LIST
    " DO NOT FORGET TO ENABLE VIA :PluginInstall
    " Periodically update via :PluginUpdate

call vundle#end()
filetype plugin indent on
" .......................................................... }}}

" SuperTab {{{1                             supertab_mark 'r
" ----------------------------------------------------------
    " Plugin 'ervandew/supertab'
" Disabling SuperTab for now, to test deoplete
" If you reenable SuperTab, you will want to tweak its settings
" to your liking, because by default it works at the beginning
" of a line...if you enable this and get confused, CTL-TAB
" inserts a literal tab...
" let g:SuperTabCompletionContexts = ['s:ContextText', 's:ContextDiscover']
" let g:SuperTabContextTextOmniPrecedence = ['&omnifunc', '&completefunc']
" let g:SuperTabContextDiscoverDiscovery =
"         \ ['&completefunc:<c-x><c-u>', '&omnifunc:<c-x><c-o>']
"
" .......................................................... }}}

" MiniMap {{{1                             minimap_mark   'q
" ----------------------------------------------------------
let g:minimap_width = 10
let g:minimap_auto_start = 1
let g:minimap_auto_start_win_enter = 1
let g:minimap_block_filetypes = ['fugitive', 'NERDtree', 'tagbar' ]
let g:minimap_block_buftypes = ['nofile', 'nowrite', 'quickfix', 'terminal', 'prompt']
" toggle MiniMap on/off with comma-m
map <leader>m :MinimapToggle<CR>
<<<<<<< HEAD

" augroup minim " {{{2
"     autocmd!
"     " Exit MiniMap on buffer close.
"     autocmd BufWinLeave * if exists('b:Minimap') | execute MinimapClose | endif
" augroup END  " }}}
=======
>>>>>>> develop
" .......................................................... }}}

" File Browser {{{1                             file_mark 'f
" ----------------------------------------------------------
" NerdTree is a file browser, comma-f
map <leader>f :NERDTreeToggle<CR>
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'

" Define a group of autocmds to trigger
" Nerdtree on events {{{2
augroup nerdtree
    autocmd!

    " If you want NERDtree to always autostart with nvim, leave this:
    " Start NERDTree and put the cursor back in the other window.
    autocmd VimEnter * NERDTree | wincmd p

    " Start NERDTree, unless a file or session is specified, eg. vim -S session_file.vim.
    "autocmd StdinReadPre * let s:std_in=1
    "autocmd VimEnter * if argc() == 0 && !exists('s:std_in') && v:this_session == '' | NERDTree | endif

    " Open the existing NERDTree on each new tab.
    autocmd BufWinEnter * silent NERDTreeMirror

    " Exit Vim if NERDTree is the only window left.
    autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif
augroup END " }}}

let g:NERDTreeGitStatusUseNerdFonts = 1
let g:NERDTreeGitStatusGitBinPath = '/usr/bin/git'
let g:NERDTreeGitStatusShowClean = 1
" .......................................................... }}}

" Autocompletion for hackers {{{1     autocompletion_mark 'a
" ----------------------------------------------------------
" Pear-tree is a quote, bracket, and tag
" pair autocompletion tool,  with intellect.
"
" In pear-tree, smart pairs are disabled by default,
" You may wish to comment these out, rtfm.
let g:pear_tree_smart_openers = 1
let g:pear_tree_smart_closers = 1
let g:pear_tree_smart_backspace = 1

" Use deoplete.
let g:deoplete#enable_at_startup = 1
call deoplete#custom#option({
    \ 'auto_complete_delay': 200,
    \ 'smart_case': v:true,
    \ })
" .......................................................... }}}

" Linting & Style {{{1                      linting_mark  's
" ----------------------------------------------------------
" Ale is a linter that works as you type.
" Look for errors in the far left column, to the left
" of the line numbers in your editor.
let g:ale_sign_column_always = 1
let g:airline#extensions#ale#enabled = 1
let g:ale_floating_window_border = ['│', '─', '╭', '╮', '╯', '╰']

let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'python': ['autoimport', 'yapf', 'black'],
\   'sh': ['shellcheck'],
\   'bash': ['shellcheck',],
\   'vimscript': ['vint'],
\   'ansible': ['ansible-lint'],
\   'yaml': ['yamllint', 'prettier'],
\   'markdown': ['markdownlint', 'vale', 'prettier'],
\   'txt': ['vale']
\}
" TODO: find a way to lint zsh and gfortran.

" Set this variable to 1 to lint and style fix files when you save them.
" This is sometimes controversial and admittedly aggressive.
" I always do this.
let g:ale_fix_on_save = 1
" .......................................................... }}}

" Etc {{{1                                       etc_mark 'e
" ----------------------------------------------------------
" Set the font properly and let's run an elegant default theme
let g:airline_powerline_fonts = 1
let g:airline_theme='base16_adwaita'
" a gorgeous comliement to iceberg, imho.

" for more colorschemes see .vim/colors
colorscheme iceberg
" because this is @seannyob's personal vimrc, the default
" theme shall always be iceberg. learn how cool it is here:
" https://github.com/cocopon/iceberg.vim
" iceberg is pure class and elegance.
" works in dark and light modes, obvs.

" ditch vim's lame insertline
" NOTE: this must be near the end of your vimrc for some reason?
set noshowmode
" .......................................................... }}}

" biblio {{{1                                 biblio_mark 'o
" ----------------------------------------------------------
" inspiration. move me brightly.
" https://www.makeuseof.com/tag/5-things-need-put-vim-config-file/
" https://github.com/VundleVim/Vundle.vim
" https://github.com/davidhalter/jedi-vim
" https://github.com/ervandew/supertab
" https://github.com/tpope/vim-eunuch
" https://medium.com/@huntie/10-essential-vim-plugins-for-2018-39957190b7a9
" https://vimawesome.com/
" https://github.com/amix/vimrc
" https://github.com/scrooloose/nerdtree
" https://github.com/dense-analysis/ale
" https://github.com/Shougo/deoplete.nvim
" https://github.com/liuchengxu/vim-which-key
" https://ebonhand.wordpress.com/2011/03/30/automatically-save-and-load-vim-views-folds/
" https://vim.fandom.com/wiki/Using_marks
" https://www.arp242.net/vim-myths.html
" https://www.arp242.net/effective-vimscript.html
" https://devhints.io/vimscript
" .......................................................... }}}

" some deep magic {{{1
" ----------------------------------------------------------

" this stuff has to go at the end:

" set_rc_marks() {{{2

" a function to set vim marks in this .vimrc file,
" so that marks are portable with the file,
" to enable jumping around by default.

" in our case, to create a Table of Contents
" with nvim's native mark jumping functionality.

" set_rc_marks() is described here with ridiculous verbosity
" as an educational tool for folks new to programming,
" vimscript, or whatevs. if you find a mistake, please
" let us know!

" set an autocmd group called `mrk` to ensure that we can
" contain things and not let multiple sourcing get heavy.
" :h autocmd for more.
augroup mrk

    " clear previous calls
    autocmd!

    " upon loading a buffer `.vimrc`,
    " in other words, whenever you load this file into nvim,
    " then run a function, let's call it set_rc_marks()
    autocmd BufReadPost .vimrc silent! call <SID>set_rc_marks()

    " declare the function named set_rc_marks()
    function! s:set_rc_marks()

        " our table of contents data is written into `rcmarks`,
        " which is a dictionary array, a kind of associative array.
        " in english, it is basically a two column table.

        " update this array if you write new top level items
        " to the Table of Contents.
        " just mirror their heading template & naming schemes for this to work.

        " anyways, an associative dictionary array in vimscript looks like:

            " key: value
            " ---: ----------------------
        let rcmarks = {
            \ 'i': 'intro_mark',
            \ 't': 'toc_mark',
            \ 'g': 'gs_mark',
            \ 'p': 'python_mark',
            \ 'k': 'leader_mark',
            \ 'm': 'motion_mark',
            \ 'b': 'tabs_mark',
            \ 'w': 'window_mark',
            \ 'l': 'plugins_mark',
            \ 'r': 'supertab_mark',
            \ 'q': 'minimap_mark',
            \ 'f': 'file_mark',
            \ 'a': 'autocompletion_mark',
            \ 's': 'linting_mark',
            \ 'e': 'etc_mark',
            \ 'o': 'biblio_mark',
            \ 'x': 'set_rc_marks'
            \ }

        " let's get down to business.

        " the left column stuff in rcmarks is the [key];
        " the right, [value];
        " use items() to return both;
        " so for each of these entries of items in the dict array called rcmarks:
        for [key, value] in items(rcmarks)

            " we could use keeppattern / here to search the file, but `ijump!`
            " makes it trivially simple to find the first instance
            " of a string, & and with bang !, it looks inside comments

            " so we just adhere to a fold header naming convention,
            " as in, toc_mark, gs_mark, and so on...
            " use those value naming conventions as a habit, and
            " set them as the value of the TOC and use the jump mark
            " letter as the [key], and we have built an ad-hoc database, basically.

            " because this function is in the .vimrc file,
            " this function must be at the end of this file to work properly,
            " so that the first instance of `toc_mark` is the actual TOC, for
            " example, as opposed to any instance of `toc_mark` in our array. <3
            " LESSON: in scripting languages, order usually matters.

            " This finds the jump mark location for the items' section.
            execute 'ijump! ' . value

            " once we're there, just set the mark
            " in nvim, marks are just letters a - zed which i picked out.
            execute 'mark ' . key

            " and we're done, and the machine will go on to the next one
            " until there are no more items in `rcmarks`.

        " business done.
        endfor

    " run fold all
    execute 'normal zM'

    " return to line 1
    :1

   " function done.
   endfunction

" close the autocmd group for friendliness.
augroup END

"      ___________________________________
"     /  Thanks for hanging in. ya rock.  \
"    | We chill @ ODU CPPO on Discord. <3  |
"     \       seannyob@icloud.com         /
"      -----------------------------------
"         \
"          \
"            .--.
"           |o_o |
"           |:_/ |
"          //   \ \
"         (| <3  | )
"        /'\_   _/`\
"        \___)=(___/

" you could wget https://bit.ly/3uKdLWy

" https://bitbucket.org/seannyob/dotfiles/
" pull requests welcome;
" you must use git flow avh;
"
" or & also we'll try to run hotfixes for you,
" if you need assisatance,
" so long as we can find time.

" email is fastest.
" sobrien@odu.edu or...
"  _____________________
" / seannyob@icloud.com \
" \  to dare is to do.  /
"  ---------------------
"          \
"           \
"              _
"            <   )
"             ) (  />>
"            (  (_/>>
"             )  />
"             ) (
"             \_ }
"            *   *
"           ( | | )
"            * _ *
"
"          ~* COYS *~
"
" peace &
" science &
" zsh &
" nvim &
" penguin.







































" Still searching, Arthur?
" The holy-grail has to be here somewhere...





































" :h holy-grail



































































































" The answer to life, the universe & everything is apparently :h 42

" EOF }}}
